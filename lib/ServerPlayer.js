const Player = require("../shared/Player.js");
const World = require("../shared/World.js");
const Item = require("../shared/Item.js");

function ServerPlayer(body) {
    this.body = body;
    this.mass = body.GetMass();
    this.items = {};
}

ServerPlayer.prototype = new Player();
ServerPlayer.constructor = Player;

/**
 * Update player
 *
 * @param deltaTime
 */
ServerPlayer.prototype.update = function(deltaTime) {
    this.handleFixedRotation();
    this.incrementOxygen();
    (this.jumpTimeOut > 0) ? this.jumpTimeOut-- : "" ;
    this.stateTime += deltaTime;
}

/**
 * Will apply force to the player based on what client sent
 * to server player.
 *
 * @param input
 */
ServerPlayer.prototype.applyInput = function(input) {
    switch(input.t) {
        case "s": // Stop
            this.stop();
            break;
        case "w": // walk
            this.walk(input);
            break;
        case "j": // jump
            this.jump();
            break;
        case "g": // get up
            this.getUp();
            break;
    }
}

/**
 * Will set the linear velocity of a player when walking.
 * And handles the oxygen use when walking
 *
 * @param goRight
 */
ServerPlayer.prototype.walk = function(impulse) {
    if(!this.isAllowedToWalk()) return;

    var velocity = this.body.GetLinearVelocity();
    if(impulse.r) {
        velocity.x = Player.MAX_VEL;
    } else {
        velocity.x = -Player.MAX_VEL;
    }
    this.body.SetLinearVelocity(velocity);
    // 10 is the max impulse for walking
    this.handleOxygenUse(Player.VEL_INC);
}

/**
 * Will apply an impulse for the player to jump if he is
 * currently standing on the ground at the jumpTimeout is not bigger than zero
 *
 * @returns {boolean}
 */
ServerPlayer.prototype.jump = function() {
    if(this.canJump()) {
        this.body.ApplyImpulse(this.calcJump(), this.body.GetWorldCenter());
        this.jumpTimeOut = Player.JUMP_TIMOUT_VAL;
        this.handleOxygenUse(300);
    }
}

/**
 * Tries to get up if allowed
 */
ServerPlayer.prototype.getUp = function() {
    if(this.canIGetUp()) {
        var impulse = this.calcForceVector(0, -10);
        this.body.ApplyImpulse(impulse, this.body.GetWorldCenter());
        var that = this;
        setTimeout(function() {
            that.body.SetAngle(0);
        }, 150);
        this.jumpTimeOut = Player.GET_UP_TIMEOUT_VAL;
    }
}

/**
 * Exports necessary data for transfer between server and client
 */
ServerPlayer.prototype.getPlayerData = function() {
    var position = this.body.GetPosition();
    var velocity = this.body.GetLinearVelocity();
    var player = {
        p: position, // position
        v: velocity, // velocity
        a: this.body.GetAngle(), // angle
        h: this.body.GetUserData().health, // health
        o: this.oxygen, // oxygen
        i: this.items // items
    }
    return player;
}

/**
 * Will subtract oxygen from a player based on how high he is on the mountain
 * and how hard the amount of work is (impulse)
 *
 * @param impulse
 */
ServerPlayer.prototype.handleOxygenUse = function(impulse) {
    // check if the player has an oxygen mask and a flask
    if(this.oxygen < 0) return;
    var scaleHeight = World.HEIGHT / World.SCALE;
    var playerHeight = (scaleHeight - this.body.GetPosition().y);
    var playerHeightPercent = playerHeight / scaleHeight;
    var oxygenUse = Math.abs((impulse * playerHeightPercent) | 0) ;
    if(!this.breatheNormal) {
        this.handleFlaskOxygenUse(1);
    } else {
        if(oxygenUse > 1) {
        this.oxygen -= (oxygenUse/2);

        }
    }
}

/**
 * Toggle a player item by searching for it in the player items array and setting
 * its active state.
 *
 * @param item
 * @returns {boolean}
 */
ServerPlayer.prototype.toggleItem = function(item) {
    var flaskActive = false;
    var maskActive = false;
    for(var i in this.items) {
        var serverItem = this.items[i];
        if(i == item.i) {
            serverItem.a = (item.a) ? 1 : 0;
        }
        if(serverItem.t == Item.TYPE_OZ_FLASK && serverItem.a == 1) {
            flaskActive = true;
            continue;
        }
        if(serverItem.t == Item.TYPE_OZ_MASK && serverItem.a == 1) {
            maskActive = true;
            continue;
        }
    }
    if(flaskActive && maskActive) {
        this.breatheNormal = false;
    } else {
        if(!this.breatheNormal) {
            this.breatheNormal = true;
        }
    }
}

/**
 * Will increment the player oxygen if it is not full.
 * Increments are based on how high the player is on the mountain.
 */
ServerPlayer.prototype.incrementOxygen = function() {
    if(this.oxygen >= Player.MAX_OXYGEN) return;
    if(!this.breatheNormal) {
        this.oxygen += 3;
        this.handleFlaskOxygenUse(3);
        return;
    }
    var scaleHeight = World.HEIGHT / World.SCALE;
    var playerHeight = (scaleHeight - this.body.GetPosition().y);
    var playerHeightPercent = playerHeight / scaleHeight;
    this.oxygen += 3 * (1-playerHeightPercent);
}

/**
 * Check if the player has active ice shoes
 *
 * @returns {boolean}
 */
ServerPlayer.prototype.hasActiveIceShoes = function() {
    for(var i in this.items) {
        var item = this.items[i];
        if(item.t == Item.TYPE_ICE_SHOE && item.a == 1) {
            return true;
        }
    }
    return false;
}

/**
 * Check if the player has an active item of the type
 * @param type
 * @returns {boolean}
 */
ServerPlayer.prototype.handleFlaskOxygenUse = function(amount) {
    for(var i in this.items) {
        var item = this.items[i];
        if(item.t == Item.TYPE_OZ_FLASK) {
            if(item.v < 0) {
                // Flask is empty use player oxygen instead and remove the item;
                delete this.items[i];
                this.breatheNormal = true;
            } else {
                item.v -= amount;
            }
        }
    }
}

/**
 * Checks if the player currently has an item
 *
 * @param type
 * @returns {boolean}
 */
ServerPlayer.prototype.hasItem = function(type) {
    for(var i in this.items) {
        var item = this.items[i];
        if(item.t == type) {
            return true;
        }
    }
    return false;
}

try {
    module.exports = ServerPlayer;
} catch(e) {
    // context does not know about the exports.
}