const World = require("../shared/World.js");
const Box2D = require("../public/scripts/libs/Box2dWeb-2.1.a.3.js").Box2D;
const Item = require("../shared/Item.js");

function ServerWorld() {
    this.init();
}

/**
 * Prototype the World master object
 * and call its constructor function
 * @type {World}
 */
ServerWorld.prototype = new World();
ServerWorld.constructor = World;

/**
 * Initializes the box2d variables using require because
 * this is server side.
 *
 * @returns {boolean}
 */
ServerWorld.prototype.initBox2d = function() {
    try {
        this.b2ContactListener = Box2D.Dynamics.b2ContactListener;
        this.b2WorldManifold = Box2D.Collision.b2WorldManifold;
        this.b2Vec2 = Box2D.Common.Math.b2Vec2;
        this.b2BodyDef = Box2D.Dynamics.b2BodyDef;
        this.b2Body = Box2D.Dynamics.b2Body;
        this.b2FixtureDef = Box2D.Dynamics.b2FixtureDef;
        this.b2Fixture = Box2D.Dynamics.b2Fixture;
        this.b2World = Box2D.Dynamics.b2World;
        this.b2PolygonShape = Box2D.Collision.Shapes.b2PolygonShape;
        this.b2CircleShape = Box2D.Collision.Shapes.b2CircleShape;
        this.b2DebugDraw = Box2D.Dynamics.b2DebugDraw;
        return true;
    } catch(e) {
        return false;
    }
}

/**
 * Add setup of player hit impulses.
 */
ServerWorld.prototype.setup = function() {
    var that = this;
    this.contactListener.PostSolve = function(contact, impulse) {
        that.setupPlayerHitImpulses(contact, impulse);
    }
    this.createItems();
}

/**
 * Create random items on the mountain the world.
 */
ServerWorld.prototype.createItems = function() {
    var that = this;
    var allowedItemsInWorld = (this.entities.length / 10) | 0;
    var itemTypes = [
        Item.TYPE_ICE_SHOE,
        Item.TYPE_OZ_FLASK,
        Item.TYPE_OZ_MASK
    ];
    var placedItems = 0;
    for(var body = this.b2dworld.GetBodyList(); body; body = body.GetNext()) {
        if(placedItems > allowedItemsInWorld) return;
        var entity = body.GetUserData();
        if(entity) {
            //if(Math.random() > 0.2) continue;
            var type = itemTypes[Math.floor(Math.random()*itemTypes.length)];
            var position = body.GetPosition();
            createItem(position, type);
            placedItems++;
        }
    }

    /**
     * Creates an item in the world, and pushes it to the world items array
     *
     * @param position
     * @param type
     */
    function createItem(position, type) {
        var id;
        do {
            id = Math.floor(Math.random() * 1000);
        } while(id in that.items);
        var entity = {
            type:"block",
            name:"item",
            id: id,
            x:(position.x*World.SCALE),
            y:(position.y*World.SCALE)-25,
            angle:0,
            width:31,
            height:31,
            isStatic: true
        }
        var body = that.createBlock(entity);
        that.items[id] = {
            body: body,
            type: type
        };
    }
}

/**
 * Get all world items
 * round to two digits to save bandwidth
 * @returns {{}}
 */
ServerWorld.prototype.getWorldItems = function() {
    var items = {}
    for(var i in this.items) {
        var item = this.items[i];
        items[i] = {
            p: {
                x: (item.body.GetPosition().x * 1000) | 0,
                y: (item.body.GetPosition().y * 1000) | 0
            },
            t: item.type
        };
    }
    return items;
}

/**
 * Measure the force of two colliding objects, and subtract the amount force
 * to the player health.
 *
 * @param contact
 * @param impulse
 */
ServerWorld.prototype.setupPlayerHitImpulses = function(contact, impulse) {
    var sumImpulse = (Math.abs(impulse.normalImpulses[0]) + Math.abs(impulse.normalImpulses[1])) | 0;
    if(sumImpulse < 55) return;

    var entityA = contact.GetFixtureA().GetBody();
    var entityB = contact.GetFixtureB().GetBody();
    var body;
    var block;
    if(entityA.GetUserData()) {
        body = entityA.GetUserData();
        block = entityB.GetUserData();
    } else if(entityB.GetUserData()) {
        body = entityB.GetUserData();
        block = entityB.GetUserData();
    }
    var impulseFactor = 8;
    // take half damage if landing on snow! Its so soft
    if(block && block.name == "snow") {
        impulseFactor /= 2;
    }
    sumImpulse *= impulseFactor;
    if(body) {
        if(sumImpulse > body.health) {
            body.health = 0;
        } else {
            body.health -= sumImpulse;
        }

    }
    impulse.normalImpulses[0] = 0;
    impulse.normalImpulses[1] = 0;
}

module.exports = ServerWorld;