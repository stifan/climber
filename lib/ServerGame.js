const Game = require("../shared/Game.js");
const World = require("../shared/World.js");

function ServerGame() {
    this.init();
}

ServerGame.prototype = new Game();
ServerGame.constructor = Game;

/**
 * Creates a new ServerWorld object
 */
ServerGame.prototype.createWorld = function() {
    this.worldObject = require("./ServerWorld.js");
    this.world = new this.worldObject();
}

/**
 * Runs a game frame on the server (update)
 *
 * @param deltaTime
 */
ServerGame.prototype.runGameFrame = function(deltaTime) {
    this.world.step(deltaTime);
}

/**
 * Adds a player to the world
 *
 * @param id
 * @returns {*}
 */
ServerGame.prototype.addPlayerToWorld = function(id) {
    return this.world.createPlayer(id, {x: 20, y: (World.HEIGHT / World.SCALE) - 2});
}

try {
    module.exports = ServerGame;
} catch(e) {
    // context does not know about the exports.
}

