function ClientWorld() {
    this.init();
    this.queuedSnowflakes = {};
    this.snowflakes = {};
}

ClientWorld.prototype = new World();
ClientWorld.constructor = World;

/**
 * Initializes the box2d variables
 *
 * @returns {boolean}
 */
ClientWorld.prototype.initBox2d = function() {
    try {
        this.b2ContactListener = Box2D.Dynamics.b2ContactListener;
        this.b2WorldManifold = Box2D.Collision.b2WorldManifold;
        this.b2Vec2 = Box2D.Common.Math.b2Vec2;
        this.b2BodyDef = Box2D.Dynamics.b2BodyDef;
        this.b2Body = Box2D.Dynamics.b2Body;
        this.b2FixtureDef = Box2D.Dynamics.b2FixtureDef;
        this.b2Fixture = Box2D.Dynamics.b2Fixture;
        this.b2World = Box2D.Dynamics.b2World;
        this.b2PolygonShape = Box2D.Collision.Shapes.b2PolygonShape;
        this.b2CircleShape = Box2D.Collision.Shapes.b2CircleShape;
        this.b2DebugDraw = Box2D.Dynamics.b2DebugDraw;

        /**
         * Create method for applying an angular impulse to body
         * this is an extention of box2d.
         *
         * @param impulse
         * @constructor
         */
        this.b2Body.prototype.ApplyAngularImpulse = function(impulse) {
            if(!this.IsAwake()) {
                this.SetAwake(true);
            }
            this.m_angularVelocity += this.m_invI * impulse;
        }
        return true;
    } catch(e) {
        return false;
    }
}

/**
 * Will update all non box2d related vars
 *
 * @param deltaTime
 * @returns {boolean}
 */
ClientWorld.prototype.update = function(deltaTime) {
    for(var id in this.players) {
        this.players[id].update(deltaTime);
    }
    this.handleSnowflakes();
}

/**
 * Create a snowflake for each queued snowflake,
 * and removes flakes that have timed out.
 */
ClientWorld.prototype.handleSnowflakes = function() {
    for(var i in this.queuedSnowflakes) {
        var snowflake = this.queuedSnowflakes[i];
        this.snowflakes[i] = this.createSnowflake(i, snowflake.position);
    }
    this.queuedSnowflakes = [];

    for(var i in this.snowflakes) {
        var flake = this.snowflakes[i];
        flake.timeout--;
        if(flake.timeout < 1) {
            this.removeBody(flake.body);
            delete this.snowflakes[i];
        }
    }
}

/**
 * Create a snowflake at a position and apply an impulse to it.
 *
 * @param idx
 * @param position
 *
 * @returns {{body: *, timeout: number}}
 */
ClientWorld.prototype.createSnowflake = function(idx, position) {
    var entity = {
        type:"block",
        name:"snowflake",
        x:(position.x*World.SCALE),
        y:(position.y*World.SCALE) - 20,
        angle: 0,
        width:5,
        height:5,
        isStatic: false
    };
    var body = this.createBlock(entity);

    var impulses = [
        {x: 0.02, y: -0.12},
        {x: -0.024, y: -0.13},
        {x: 0.0242, y: -0.12},
        {x: -0.0238, y: -0.13},
        {x: 0.0235, y: -0.1},
        {x: -0.0232, y: -0.09},
        {x: -0.0242, y: -0.105},
        {x: -0.025, y: -0.1}
    ]
    var impulse = impulses[Math.floor(Math.random() * impulses.length)];
    body.ApplyImpulse(new this.b2Vec2(impulse.x,impulse.y), body.GetWorldCenter());
    body.ApplyAngularImpulse((0.01*MathHelper.DEG_TO_RAD));
    return {body: body, timeout: 200};
}

/**
 * WHen hitting the snow, q two snowflakes *
 *
 * @param position
 */
ClientWorld.prototype.hitSnow = function(position) {
    this.queuedSnowflakes.push({position: position});
    this.queuedSnowflakes.push({position: position});
}

