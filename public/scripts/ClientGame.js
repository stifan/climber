function ClientGame(socket) {
    this.socket = socket;
    this.animationFrame = null;
    this.lastUpdateTime = null;
    this.frames = 0;
    this.startTime = 0;
    this.canvas = document.getElementById("canvas");
    this.context = this.canvas.getContext('2d');
    this.world = new ClientWorld();
    this.simulatorWorld = new SimulatorWorld();
    this.chat = new Chat(this.socket, this.context);
    this.myIndex = null;
    this.items = {};

    this.camera = new Camera(
        this.canvas.width,
        this.canvas.height
    );
    this.WorldRenderer = new WorldRenderer(this.context, this.camera, this.world);
    this.UIRenderer = null;
    this.sequenceNum = 0;
    this.pendingInputs = [];

    var that = this;
    setupAnimationFrame();

    Assets.init();

    /**
     * Setup two functions to handle different browser names of requestAnimationFrame
     * and cancelAnimationFrame
     */
    function setupAnimationFrame() {
        (function() {
            var lastTime = 0;
            var vendors = ['ms', 'moz', 'webkit', 'o'];
            for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
                window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
                window.cancelAnimationFrame =
                    window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
            }

            if (!window.requestAnimationFrame)
                window.requestAnimationFrame = function(callback, element) {
                    var currTime = new Date().getTime();
                    var timeToCall = Math.max(0, 16 - (currTime - lastTime));
                    var id = window.setTimeout(function() { callback(currTime + timeToCall); },
                        timeToCall);
                    lastTime = currTime + timeToCall;
                    return id;
                };

            if (!window.cancelAnimationFrame)
                window.cancelAnimationFrame = function(id) {
                    clearTimeout(id);
                };
        }());
    }

    /**
     * will alter the keys array to toggle what key is currently pressed
     */
    function setupKeyboardEventListeners() {
        var player = that.world.players[that.myIndex];
        that.canvas.addEventListener("keydown", function(e) {
            if(that.chat.isOpen) {
                if(e.which == 8) { // return
                    that.chat.popLastLetter();
                }
                return;
            }

            if(player.keys.keyCodes[e.keyCode] && !player.keys.keyCodes[e.keyCode].down) {
                player.keys.keyCodes[e.keyCode].down = true;
            }
            e.preventDefault();
        }, false);

        that.canvas.addEventListener("keyup", function(e) {
            if(!that.chat.isOpen) {
                // check if it is one of the 1-5 keys
                if(e.which > 48 && e.which < 54) {
                    var itemNumber = e.which-48;
                    // try to find player item  from key
                    var i = 1;
                    for(var idx in player.items) {
                        var item = player.items[idx];
                        if(i == itemNumber) {
                            item.active = !item.active;
                            if(that.socket && that.socket.readyState == 1) {
                                var input = {
                                    t: "ti",
                                    d: {
                                        i: idx,
                                        a: (item.active) ? 1 : 0
                                    }
                                };
                                that.socket.send(JSON.stringify(input));
                            }
                            return;
                        }
                        i++;
                    }
                }
            }
            if(e.which == 13) {
                that.chat.toggleChat();
                return;
            }
            if(e.keyCode == Keys.ARROW_LEFT || e.keyCode == Keys.ARROW_RIGHT) {
                if(player.stop()) {
                    that.sendInput(0, {t: "s"});
                }
            }
            if(player.keys.keyCodes[e.keyCode] && player.keys.keyCodes[e.keyCode].down) {
                player.keys.keyCodes[e.keyCode].down = false;
            }
        }, false);

        that.canvas.addEventListener("keypress", function(e) {
            if(that.chat.isOpen) {
                that.chat.appendLetterToText(String.fromCharCode(e.which));
            }
        }, false);
    }

    /**
     * Privileged function for setting up the KeyBoard listeners
     * when client player is created.
     */
    this.setupKeyListeners = function() {
        setupKeyboardEventListeners();
    }

    // Start the animation loop
    this.animate();
}

ClientGame.prototype = new Game();
ClientGame.constructor = Game;

/**
 * Will run the animation loop and calculate deltaTime (the diff between this and last frame
 */
ClientGame.prototype.animate = function() {
    var currentTime = new Date().getTime();
    var deltaTime = 0;
    var that = this;
    if(this.lastUpdateTime) {
        deltaTime = (currentTime - this.lastUpdateTime) / 1000;
        // lock the time step to no bigger than 1/30 th of a second
        // because it will mess up box2d with higher times
        if(deltaTime > 2/60) {
            deltaTime = 2/60;
        }
        this.callKeyEvents(deltaTime);
        this.chat.update(deltaTime);
        this.world.step(deltaTime);
    }
    this.lastUpdateTime = currentTime;
    this.context.clearRect(0, 0, that.canvas.width, that.canvas.height);
    if(this.world.players[this.myIndex]) {
        var playerPosition = this.world.players[this.myIndex].body.GetPosition();
        this.camera.handlePanning(playerPosition);
    }
    this.WorldRenderer.render();
    if(this.UIRenderer) {
        this.UIRenderer.render();
    }
    this.logFrame();
    this.animationFrame = window.requestAnimationFrame(this.animate.bind(this));
}

/**
 * Will loop through the keys array to check what keys are currently down
 * and call their actions.
 */
ClientGame.prototype.callKeyEvents = function(deltaTime) {
    var player = this.world.players[this.myIndex];
    if(player) {
        for(var key in player.keys.keyCodes) {
            if(player.keys.keyCodes[key].down) {
                // If action was successful send the input
                var force = player.keys.keyCodes[key].action(player, deltaTime);
                if(force) {
                    this.sendInput(deltaTime, force);
                }
            }
        }
    }
}

/**
 * Counts number of frames per second and writes that to the client
 */
ClientGame.prototype.logFrame = function() {
    this.frames++;
    if(new Date().getTime() - this.startTime >= 1000) {
        $("#fps").text("FPS: " + this.frames);
        this.frames = 0;
        this.startTime = new Date().getTime();
    }
}

/**
 * Handles a Client message and either creates a new player if the
 * player is not in the world, else it updates the player.
 *
 * @param message
 */
ClientGame.prototype.handleClientMessage = function(message) {
    this.handlePlayers(message);
    this.handleItems(message.items);
}

/**
 * Handle the world items, add new items to the world
 * and remove items which are not sent by the game.
 *
 * @param items
 */
ClientGame.prototype.handleItems = function(items) {
    // Add new items to the world
    for(var i in items) {
        var item = items[i];
        // check if items is already in the array
        if(i in this.world.items) {
            this.world.items[i].p = item.p;
        } else {
            this.world.items[i] = item;
        }
    }
    // check for items in the world that are not sent by game
    for(var i in this.world.items) {
        if(!(i in items)) {
            delete this.world.items[i];
        }
    }
}
/**
 * Handle message players
 *
 * @param message
 */
ClientGame.prototype.handlePlayers = function(message) {
    this.createPlayers(message);
    this.removePlayers(message);
}

/**
 * Create new players and add them to the world.
 * Creates a new main player of it is current player index
 * Creates a other player if the current player idx is not
 * the client index.
 *
 * @param message
 */
ClientGame.prototype.createPlayers = function(message) {
    for(var idx in message.players) {
        if(!this.world.players[idx]) {
            if(idx == message.myIndex) {
                // Create new main player
                var player = this.createMainPlayer(idx, message);
            } else {
                // Create new Other Player
                var player = new ClientOtherPlayer(this.world.createPlayer(idx, message.players[idx].player.position), new Keys());
                this.world.players[idx] = player;
            }
            player.b2Vec2 = this.world.b2Vec2;
        } else {
            this.updatePlayer(idx, message);
        }
    }
}

/**
 * Remove clients in the removes array of the message.
 *
 * @param message
 */
ClientGame.prototype.removePlayers = function(message) {
    for(var idx in message.removes) {
        var id = message.removes[idx].i;
        if(this.world.players[id]) {
            this.world.removeBody(this.world.players[id].body);
            delete this.world.players[id];
        }
    }
}

/**
 * Creates a new Main player, set up the UI renderer and
 * adds the player to the world players array.
 *
 * Also adds the player to the simulator world
 * @param idx
 * @param message
 * @returns {ClientPlayer}
 */
ClientGame.prototype.createMainPlayer = function(idx, message) {
    this.myIndex = idx;
    var player = new ClientPlayer(this.world.createPlayer(idx, message.players[idx].player.position), new Keys());
    this.UIRenderer = new UIRenderer(this.context,  this.camera, player, this.chat);
    this.world.players[idx] = player;
    this.setupKeyListeners();
    // Add body to Simulator world
    this.simulatorWorld.players[idx] = this.simulatorWorld.createPlayer(idx, message.players[idx].player.position);

    return player;
}

/**
 * Update a player.
 * save the last processed input if it is a main player
 *
 * @param idx
 * @param message
 */
ClientGame.prototype.updatePlayer = function(idx, message) {
    // Update the player
    var lastProcessedInput = null;
    if(idx == message.myIndex) {
        // make sure lpi cannot be null, but 0 of no lpi
        lastProcessedInput = (message.lpi) ? message.lpi : 0;
        this.world.serverPlayerPosition = message.players[idx].player.position;
        this.world.serverAngle = message.players[idx].player.angle;
    }
    this.processPlayerMessage(idx, lastProcessedInput, message.players[idx], message.time);
}

/**
 * Process a server message for the client player
 *
 * Will apply server update to player and then
 * apply the inputs that are missing between
 * server message state and current client state.
 *
 * This is only applied to client player
 *
 * Server Recon!
 *
 * @param lastProcessedInput
 * @param player
 */
ClientGame.prototype.processPlayerMessage = function(idx, lastProcessedInput, playerData, serverTime) {
    var player = this.world.players[idx];
    this.handlePlayerItems(player, playerData.player.i);
    var pendingInputs = null;
    if(idx != this.myIndex) {
        pendingInputs = playerData.ai;
    } else {
        var latency = Date.now() - serverTime;
        var pendingInputsLength = this.pendingInputs.length;
        if(pendingInputsLength > 0) {
            var lastInput = this.pendingInputs[pendingInputsLength-1];
            if(lastInput.d.t == "j") {
                if(lastInput.s > lastProcessedInput) {
                    playerData = this.simulatorWorld.simulateJump(idx, playerData, lastInput);
                } else {
                    playerData = this.simulatorWorld.simulate(idx, playerData, latency);
                }
            }
        } else {
            if(playerData.player.velocity.x != 0 || playerData.player.velocity.y != 0) {
                playerData = this.simulatorWorld.simulate(idx, playerData, latency);
            }
        }
    }
    player.serverUpdate(playerData, pendingInputs);
    if(!lastProcessedInput) return;
    this.handlePendingInputs(player, lastProcessedInput);
}

/**
 * Handle the last processed player inputs
 * applies inputs which the server has not handled yet.
 *
 * Removes inputs which the server has processed.
 *
 * @param player
 * @param lastProcessedInput
 */
ClientGame.prototype.handlePendingInputs = function(player, lastProcessedInput) {
    var j = 0;
    while(j < this.pendingInputs.length) {
        var input = this.pendingInputs[j];
        if(input.s <= lastProcessedInput) {
            // input already processed. Remove it
            this.pendingInputs.splice(j, 1);
        } else {
            // Apply the input as server has not applied it yet
            player.applyInput(input);
            j++;
        }
    }
}

/**
 * Handle the message player items.
 *
 * Update items already there, based on active or not
 * Create new items just picked up
 * Delete items which is no longer in the server player inventory
 *
 * @param player
 * @param items
 */
ClientGame.prototype.handlePlayerItems = function(player, items) {
    for(var idx in items) {
        // If the item already exists update it
        if((idx in player.items)) {
            var item = player.items[idx];
            item.active = (items[idx].a) ? true : false;
            if(item.content) {
                item.content = (items[idx].v);
            }
        } else {
            // else create a new item
            player.items[idx] = new Item(items[idx].t);
        }
    }
    for(var idx in player.items) {
        if(!(idx in items)) {
            delete player.items[idx];
        }
    }
}

/**
 * Send an input to the server
 * and saves the input for later use in pending inputs
 *
 * @param data
 */
ClientGame.prototype.sendInput = function(time, data) {
    data.dt = time;
    if(this.socket && this.socket.readyState == 1) {
        var input = {
            t: "i",
            s: this.sequenceNum++,
            d: data
        };
        this.socket.send(JSON.stringify(input));
        // save current position in input
        input.deltaTime = time;
        input.time = Date.now();
        this.pendingInputs.push(input);
    }
}

/**
 * Stops the animation loop
 */
ClientGame.prototype.stop = function() {
    window.cancelAnimationFrame(this.animationFrame);
}

