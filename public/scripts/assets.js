function Assets() {}

Assets.init = function() {
    Assets.hero = new Image();
    Assets.hero.src = "img/hero.png";

    Assets.oxygenMask = new Image();
    Assets.oxygenMask.src = "img/oz_mask.png";

    Assets.oxygenFlask = new Image();
    Assets.oxygenFlask.src = "img/oz_flask.png";

    Assets.iceShoes = new Image();
    Assets.iceShoes.src = "img/ice_shoes.png";

    Assets.itemBar = new Image();
    Assets.itemBar.src = "img/item_bar.png";

    Assets.icons = new Image();
    Assets.icons.src = "img/icons.png";
}