function Camera(width, height) {
    // Height and width of canvas
    this.height = height;
    this.width = width;

    // Camera Offset
    this.offsetX = 0;
    this.offsetY = 0;
    this.maxOffsetX = World.WIDTH-width;
    this.maxOffsetY = World.HEIGHT-height;
    this.buffer = 150;
}

/**
 * Will check if the user is outside the no panning zone
 * and then pan to him
 * @param position
 */
Camera.prototype.handlePanning = function(position) {
    var worldX = position.x*World.SCALE;
    var worldY = position.y*World.SCALE;
    var canvasX = worldX - this.offsetX;
    var canvasY = worldY - this.offsetY;

    this.panTo(worldX,worldY, canvasX, canvasY);
}

/**
 * Moves canvas to new position world position
 * if the buffer has not been reached
 *
 * @param worldX
 * @param worldY
 * @param canvasX
 * @param canvasY
 */
Camera.prototype.panTo = function(worldX, worldY, canvasX, canvasY) {

    if(this.offsetX <= this.maxOffsetX && this.offsetX >= 0) {
        if(canvasX < this.buffer) {
            this.offsetX = worldX-this.buffer;
        } else if(canvasX > this.width-this.buffer) {
            this.offsetX = worldX-(this.width-this.buffer);
        }
    }

    if(this.offsetY <= this.maxOffsetY && this.offsetY >= 0) {
        if(canvasY < this.buffer) {
            this.offsetY = worldY-this.buffer;
        } else if(canvasY > this.height - this.buffer) {
            this.offsetY = worldY-(this.height - this.buffer);
        }
    }

    this.checkBounds();
}

/**
 * Checks that the camera is not out of bounds
 * sets camera to limit of OB
 */
Camera.prototype.checkBounds = function() {
    if(this.offsetX < 0) {
        this.offsetX = 0;
    }

    if(this.offsetX > this.maxOffsetX) {
        this.offsetX = this.maxOffsetX;
    }

    if(this.offsetY < 0) {
        this.offsetY = 0;
    }

    if(this.offsetY > this.maxOffsetY) {
        this.offsetY = this.maxOffsetY;
    }
}