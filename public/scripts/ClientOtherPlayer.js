function ClientOtherPlayer(body, keys) {
    this.body = body;
    this.mass = body.GetMass();
    this.keys = keys;
    this.pendingInputs = [];
    this.pendingJump = false;
    this.delayedAngle = null;
    this.delayedPosition = null;
    this.delayedVelocity = null;
    this.queuedAngle = null;
    this.queuedPosition = null;
    this.queuedVelocity = null;
    this.items = {};
}

ClientOtherPlayer.prototype = new ClientPlayer(null, null);
ClientOtherPlayer.constructor = Player;

/**
 * Updates non box2d properties of the player on each step
 */
ClientOtherPlayer.prototype.update = function(deltaTime) {
    this.handleFixedRotation();
    this.checkFalling();
    this.ApplyPendingInputs(deltaTime);
    (this.jumpTimeOut > 0) ? this.jumpTimeOut-- : "" ;
    this.stateTime += deltaTime;
}

/**
 * Applies server player data to player object
 *
 * @param data
 */
ClientOtherPlayer.prototype.serverUpdate = function(data, pendingInputs) {
    this.delayedAngle = data.player.angle;
    this.delayedPosition = data.player.position;
    this.delayedVelocity = data.player.velocity;

    this.pendingInputs = pendingInputs;
    this.health = data.player.h;
    this.oxygen = data.player.o;
    this.chatMessage = data.chatMessage;
    this.applyInterpolation();
}

/**
 * Will apply an impulse for the player to jump if he is
 * currently standing on the ground at the jumpTimeout is not bigger than zero
 *
 * @returns {boolean}
 */
ClientOtherPlayer.prototype.jump = function() {
    if(this.canJump()) {
        this.body.ApplyImpulse(this.calcJump(), this.body.GetWorldCenter());
        this.jumpTimeOut = Player.JUMP_TIMOUT_VAL;
    }
}

/**
 * Tries to get up if allowed
 */
ClientOtherPlayer.prototype.getUp = function() {
    if(this.canIGetUp()) {
        var impulse = this.calcForceVector(0, -10);
        this.body.ApplyImpulse(impulse, this.body.GetWorldCenter());
        var that = this;
        setTimeout(function() {
            that.body.SetAngle(0);
        }, 150);
        this.jumpTimeOut = Player.GET_UP_TIMEOUT_VAL;
    }
}

/**
 * Applies pending inputs and then applies the queued position and angle
 */
ClientOtherPlayer.prototype.applyInterpolation = function() {
    if(!this.delayedPosition || this.pendingInputs.length > 0) return;
    if(this.pendingJump) {
        this.jump();
        this.pendingJump = false;
    }
    // make sure there is a queued position
    // from last game update before applying it
    if(this.queuedPosition) {
        this.body.SetPosition(this.queuedPosition);
        this.body.SetAngle(this.queuedAngle);
        this.body.SetLinearVelocity(this.queuedVelocity);
    }
    this.queuedAngle = this.delayedAngle;
    this.queuedPosition = this.delayedPosition;
    this.queuedVelocity = this.delayedVelocity;
}

/**
 * Apply then pending inputs if there is some
 *
 * When there are no more pending inputs, apply the delayed position
 *
 * @param deltaTime
 * @constructor
 */
ClientOtherPlayer.prototype.ApplyPendingInputs = function(deltaTime) {
    if(this.pendingInputs.length < 1) return;
    var that = this;
    var input = this.pendingInputs.shift();
    switch(input.t) {
        case "s":
            this.stop();
            break;
        case "w":
            this.walk(input.r);
            applyDelayedData();
            break;
        case "j":
            this.pendingJump = true;
            break;
        case "g":
            this.getUp();
            break;
        default:
            console.log("unknown interpolation input type");
            break;
    }

    function applyDelayedData() {
        // After applying the input, check if pending inputs is empty again
        if(that.pendingInputs.length < 1) {
            // if it is, apply the queued position and angle.
            that.body.SetPosition(that.delayedPosition);
            that.body.SetAngle(that.delayedAngle);
            that.body.SetLinearVelocity(that.delayedVelocity);
            that.queuedPosition = null;
            that.queuedAngle = null;
            that.queuedVelocity = null;
        }
    }
}

