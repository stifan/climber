function ClientPlayer(body, keys) {
    this.body = body;
    this.mass = null;
    if(body) {
        this.mass = this.body.GetMass();
    }
    this.direction = ClientPlayer.FACING_RIGHT;
    this.stateTime = 0;
    this.animationIndex = 0;
    this.keys = keys;
    this.b2Vec2 = Box2D.Common.Math.b2Vec2;
    this.items = {};
}
ClientPlayer.prototype = new Player();
ClientPlayer.constructor = Player;

/**
 * Static Player Vars
 */
ClientPlayer.FACING_RIGHT = 0;
ClientPlayer.FACING_LEFT = 1;
ClientPlayer.WALKING_FRAMES = 15;
ClientPlayer.CRAWLING_FRAMES = 12;
ClientPlayer.FALLING_FRAMES = 11;
ClientPlayer.PIXEL_WIDTH = 128;
ClientPlayer.PIXEL_HEIGHT = 183;

/**
 * Updates non box2d properties of the player on each step
 */
ClientPlayer.prototype.update = function(deltaTime) {
    this.handleFixedRotation();
    this.checkFalling();
    (this.jumpTimeOut > 0) ? this.jumpTimeOut-- : "" ;
    this.stateTime += deltaTime;
}

/**
 * Applies server player data to player object
 *
 * @param data
 */
ClientPlayer.prototype.serverUpdate = function(data, pendingInputs) {
    this.body.SetPosition(data.player.position);
    this.body.SetAngle(data.player.angle);
    this.health = data.player.h;
    this.oxygen = data.player.o;
    this.chatMessage = data.chatMessage;
}

/**
 * Will push the player to left or right until the desired velocity has been reached
 *
 * @param goRight
 * @param deltaTime
 * @returns {*}
 */
ClientPlayer.prototype.walk = function(goRight) {
    if(!this.isAllowedToWalk()) return null;
    this.setDirection();
    this.handleAnimationIndex(
        0,
        ClientPlayer.WALKING_FRAMES,
        (this.stateTime > (0.3 / 10))
    );
    var velocity = this.body.GetLinearVelocity();
    if(goRight) {
        velocity.x = Player.MAX_VEL;
    } else {
        velocity.x = -Player.MAX_VEL;
    }
    this.body.SetLinearVelocity(velocity);
    return {
        t: "w",
        r: goRight
    };
}

/**
 * Jump if allowed
 * @returns {*}
 */
ClientPlayer.prototype.jump = function() {
    if(this.canJump()) {
        var impulse = this.calcJump();
        this.body.ApplyImpulse(impulse, this.body.GetWorldCenter());
        this.jumpTimeOut = Player.JUMP_TIMOUT_VAL;
        return {
            t: "j"
        };
    }
    return null;
}

/**
 * Tries to get up if allowed
 *
 * @returns {{x: (Box2D.Common.Math.b2Vec2.x), y: (Box2D.Common.Math.b2Vec2.y), t: string}}
 */
ClientPlayer.prototype.getUp = function() {
    if(this.canIGetUp()) {
        var impulse = this.calcForceVector(0, -10);
        this.body.ApplyImpulse(impulse, this.body.GetWorldCenter());
        var that = this;
        setTimeout(function() {
            that.body.SetAngle(0);
        }, 150);
        this.jumpTimeOut = Player.GET_UP_TIMEOUT_VAL;

        return {
            x: impulse.x,
            y: impulse.y,
            t: "g"
        }
    }
    return null;
}

/**
 * Will set the player direction based on current velocity
 *
 * Only set direction when speed is above a certain point
 */
ClientPlayer.prototype.setDirection = function() {
    var vel = this.body.GetLinearVelocity();
    if(vel.x > 0.5 && this.direction != ClientPlayer.FACING_RIGHT) {
        this.direction = ClientPlayer.FACING_RIGHT;
    }

    if(vel.x < -0.5 && this.direction != ClientPlayer.FACING_LEFT) {
        this.direction = ClientPlayer.FACING_LEFT;
    }
}

/**
 * Will handle the animation index based on the min and max values
 * increments and resets when a condition is true.
 *
 * @param min
 * @param max
 * @param condition
 */
ClientPlayer.prototype.handleAnimationIndex = function(min, max, condition) {
    if(this.animationIndex < min) {
        this.animationIndex = min;
    }

    if(condition) {
        this.animationIndex++;
        this.stateTime = 0;
    }

    if(this.animationIndex >= max) {
        this.animationIndex = min;
    }
}

/**
 * Will check if the player is currently falling by checking his fixtures.
 * Will start falling animation when the player has reached a certain speed.
 */
ClientPlayer.prototype.checkFalling = function() {
    if(!this.fixtureUnderFoot && !this.fixtureOnRight && !this.fixtureOnLeft) {
        this.handleAnimationIndex(
            ClientPlayer.WALKING_FRAMES+ClientPlayer.CRAWLING_FRAMES,
            ClientPlayer.WALKING_FRAMES+ClientPlayer.CRAWLING_FRAMES+ClientPlayer.FALLING_FRAMES,
            (this.stateTime > 1/60 && this.body.GetLinearVelocity().y > 6)
        );
    }
}

/**
 * Apply a missing input for server reconciliation.
 *
 * @param input
 */
ClientPlayer.prototype.applyInput = function(input) {
    if(input.d.t == "j"){
        this.jump();
    } else if(input.d.t == "w") {
        var speed = Player.MAX_VEL;
        if(!input.d.r) {
            speed = -Player.MAX_VEL;
        }
        var moveDistance = input.deltaTime * speed;
        var position = this.body.GetPosition();
        position.x += moveDistance;
        this.body.SetPosition(position);
    }

}

ClientPlayer.prototype.hasActiveIceShoes = function() {
    for(var i in this.items) {
        var item = this.items[i];
        if(item.type == Item.TYPE_ICE_SHOE && item.active) {
            return true;
        }
    }
    return false;
}