function SimulatorWorld() {
    this.init();
    this.stepTime = 2/60;
}

SimulatorWorld.prototype = new World();
SimulatorWorld.constructor = World;
/**
 * Initializes the box2d variables
 *
 * @returns {boolean}
 */
SimulatorWorld.prototype.initBox2d = function() {
    try {
        this.b2ContactListener = Box2D.Dynamics.b2ContactListener;
        this.b2WorldManifold = Box2D.Collision.b2WorldManifold;
        this.b2Vec2 = Box2D.Common.Math.b2Vec2;
        this.b2BodyDef = Box2D.Dynamics.b2BodyDef;
        this.b2Body = Box2D.Dynamics.b2Body;
        this.b2FixtureDef = Box2D.Dynamics.b2FixtureDef;
        this.b2Fixture = Box2D.Dynamics.b2Fixture;
        this.b2World = Box2D.Dynamics.b2World;
        this.b2PolygonShape = Box2D.Collision.Shapes.b2PolygonShape;
        this.b2CircleShape = Box2D.Collision.Shapes.b2CircleShape;
        this.b2DebugDraw = Box2D.Dynamics.b2DebugDraw;
        return true;
    } catch(e) {
        return false;
    }
}

SimulatorWorld.prototype.init = function() {

    if(this.initBox2d()) {
        var gravity = new this.b2Vec2(0, World.GRAVITY);
        var allowSleep = false;
        this.b2dworld = new this.b2World(gravity, allowSleep);
        this.contactListener = new this.b2ContactListener;
        this.createWorld();
        this.setupContactListeners();
        this.b2dworld.SetContactListener(this.contactListener);
        return true;
    } else {
        console.log("Could not initialize box2d do not continue creating the world!");
        return false;
    }
}

/**
 * Step the world using "abstract" update method
 * and stepping the box2d world.
 *
 * @param deltaTime
 */
SimulatorWorld.prototype.step = function(idx, deltaTime, i) {
    // velocity iterations = 8
    // position iterations = 3
    var playerBody = this.players[idx];
    playerBody.SetActive(true);
    this.b2dworld.Step(deltaTime, 8, 3);
    this.b2dworld.ClearForces();
}

/**
 * Run a fixed amount of simulation frames as fast as possible
 * Delta time is fixed to 30 and then simulate the amount missing
 * frames based on the server latency.
 *
 * @param idx
 * @param playerData
 * @param latency
 * @returns {*}
 */
SimulatorWorld.prototype.simulate = function(idx, playerData, latency) {
    var frames = (latency / 1000/this.stepTime) | 0;
    var body = this.players[idx];
    if(body) {
        var playerPosition = playerData.player.position;
        body.SetPosition(playerPosition);
        body.SetAngle(playerData.player.angle);
        body.SetLinearVelocity(playerData.player.velocity);
        for(var i = 0; i < frames; i++) {
            this.step(idx, this.stepTime, i);
        }
        playerData.player.position = body.GetPosition();
        playerData.player.velocity = body.GetLinearVelocity();
    }
    return playerData;
}

/**
 * Simulate a jump, by setting the player velocity to the start jump velocity
 * and then simulate for as long since the last jump based on now and when the
 * jump was applied.
 *
 * @param idx
 * @param playerData
 * @param input
 * @returns {*}
 */
SimulatorWorld.prototype.simulateJump = function(idx, playerData, input) {
    // apply the jump initial velocity
    playerData.player.velocity.y = -11;
    var timeSinceJump = Date.now() - input.time;
    return this.simulate(idx, playerData, timeSinceJump);
}

/**
 * Setup one way collision for simulation world
 */
SimulatorWorld.prototype.setupContactListeners = function() {
    var that = this;
    this.contactListener.BeginContact = function(contact) {
        that.setupOneWayCollision(contact);
    }
    this.contactListener.EndContact = function(contact) {
        contact.SetEnabled(true);
    }
}