function Chat(socket, context) {
    this.isOpen = false;
    this.showBlinker = true;
    this.text = "";
    this.stateTime = 0;
    this.socket = socket;
    this.c = context;
}
Chat.TEXT_FIELD_HEIGHT = 25;
Chat.TEXT_FIELD_WIDTH = 300;
/**
 * Update the chat, toggle the blinker
 *
 * @param deltaTime
 */
Chat.prototype.update = function(deltaTime) {
    if(!this.isOpen) return;

    if(this.stateTime < 0.6) {
        this.showBlinker = true;
    } else {
        this.showBlinker = false;
    }

    if(this.stateTime > 1.2) {
        this.stateTime = 0;
    }
    this.stateTime += deltaTime;
}

/**
 * Toggle the chat
 * if open its closed and vice versa
 */
Chat.prototype.toggleChat = function() {
    if(this.isOpen) {
        this.close();
    } else {
        this.open();
    }
}

/**
 * Set chat to be open
 */
Chat.prototype.open = function() {
    this.isOpen = true;
}

/**
 * Close the chat and send what it is in it
 * or just close it.
 */
Chat.prototype.close = function() {
    // check if there is text in the chat, if there display is
    var text = this.text;
    if(text.trim().length > 0) {
        if(this.socket && this.socket.readyState == 1) {
            this.socket.send(JSON.stringify({
                t: "cm",
                d: text
            }));
        }
        console.log("Send message");
    } else {
        console.log("close chat, no text", text);
    }
    this.text = "";
    this.isOpen = false;
}

/**
 * Appends a letter to the current chat text,
 * and resets the blinker stateTime
 * @param letter
 */
Chat.prototype.appendLetterToText = function(letter) {
    this.stateTime = 0;
    var textLength = this.c.measureText(this.text);
    if(textLength.width < 261) {
        this.text += "" + letter;
    }
}

/**
 * Pops the last letter of the chat string, if it has character
 */
Chat.prototype.popLastLetter = function() {
    if(this.text.length > 0) {
        this.text = this.text.substring(0, this.text.length - 1);
    }
}