function WorldRenderer(context, camera, world) {
    this.c = context;
    this.camera = camera;
    this.world = world;
}

WorldRenderer.prototype = new Renderer();
WorldRenderer.constructor = Renderer;

/**
 * Render the world
 */
WorldRenderer.prototype.render = function() {
    this.renderBackground();
    this.renderMountain();
    this.renderItems();
    if(this.world.serverPlayerPosition) {
        var position = this.world.serverPlayerPosition;
        this.translateCanvasTo(position, this.world.serverAngle);
        this.c.beginPath();
        this.c.fillStyle = "black";
        this.c.rect(-20, -37, 40, 74);
        this.c.fill();
        this.c.restore();
    }
    this.renderPlayers();
    this.renderSnowFlakes();
    this.renderSnowBlocks();
}

/**
 * Renders a background mountain, which is just a big triangle.
 */
WorldRenderer.prototype.renderBackground = function() {
    // Save the canvas state
    this.c.save();
    this.c.fillStyle = "#333";
    this.c.beginPath();
    this.c.moveTo(0-this.camera.offsetX, World.HEIGHT-this.camera.offsetY);
    this.c.lineTo((World.WIDTH/2)-this.camera.offsetX, 0-this.camera.offsetY);
    this.c.lineTo(World.WIDTH-this.camera.offsetX, World.HEIGHT-this.camera.offsetY);
    this.c.fill();

    this.c.restore();
}

/**
 * Will iterate through all blocks in the box2d world and draw each of them.
 */
WorldRenderer.prototype.renderMountain = function() {
    for(var body = this.world.b2dworld.GetBodyList(); body; body = body.GetNext()) {
        var entity = body.GetUserData();
        if(entity) {
            if(!this.isBlockInView(entity.width, entity.height, body.GetPosition())) continue;
            this.drawBody(entity, body.GetPosition(), body.GetAngle());
        }
    }
}

WorldRenderer.prototype.renderSnowFlakes = function() {
    for(var i in this.world.snowflakes) {
        var flake = this.world.snowflakes[i];
        var entity = flake.body.GetUserData();
        this.drawBody(entity, flake.body.GetPosition(), flake.body.GetAngle());
    }
}
/**
 * Renders all snow blocks
 */
WorldRenderer.prototype.renderSnowBlocks = function() {
    for(var body = this.world.b2dworld.GetBodyList(); body; body = body.GetNext()) {
        var entity = body.GetUserData();
        if(entity && entity.name == "snow") {
            if(!this.isBlockInView(entity.width, entity.height, body.GetPosition())) continue;
            this.drawSnow(entity, body.GetPosition(), body.GetAngle());
        }
    }
}

/**
 * Render all mountain items
 */
WorldRenderer.prototype.renderItems = function() {
    var draws = 0;
    for(var i in this.world.items) {
        var item = this.world.items[i];
        var iconSize = UIRenderer.ICON_SIZE+5;
        if(!this.isBlockInView(iconSize, iconSize, item.p)) continue;
        var itemOffset = item.t * UIRenderer.ICON_SIZE;
        this.translateCanvasTo(item.p, 0);
        this.c.drawImage(
            Assets.icons,
            0,
            itemOffset,
            UIRenderer.ICON_SIZE,
            UIRenderer.ICON_SIZE,
            -iconSize/2,
            -iconSize/2,
            iconSize,
            iconSize
        );
        this.c.restore();
        draws++;
    }
    $("#itemDraws").text("item draws: " + draws);
}

/**
 * Check if the block is within the view of the player
 *
 * @param entity
 * @param position
 * @returns {boolean}
 */
WorldRenderer.prototype.isBlockInView = function(width, height, position) {
    if((position.x*World.SCALE) < this.camera.offsetX-width/2) {
        return false;
    }

    if((position.x * World.SCALE)-width/2 > this.camera.offsetX+this.camera.width) {
        return false;
    }

    if(position.y*World.SCALE < this.camera.offsetY-height/2) {
        return false;
    }

    if((position.y * World.SCALE)-height/2 > this.camera.offsetY+this.camera.height+30) {
        return false;
    }

    return true;
}

/**
 * Renders the player and if he has a chat message draw a speech bubble.
 *
 * @param player
 */
WorldRenderer.prototype.renderPlayer = function(idx, player) {
    var position = player.body.GetPosition();
    var angle = player.body.GetAngle();
    this.translateCanvasTo(position, angle);
    var directionOffset = player.direction * ClientPlayer.PIXEL_WIDTH;
    var animationOffset = player.animationIndex * ClientPlayer.PIXEL_HEIGHT;
    this.c.drawImage(
        Assets.hero,
        directionOffset,
        animationOffset,
        ClientPlayer.PIXEL_WIDTH-1,
        ClientPlayer.PIXEL_HEIGHT-1,
        -26,
        -38,
        ClientPlayer.PIXEL_WIDTH * 0.41,
        ClientPlayer.PIXEL_HEIGHT * 0.41
    );
    this.renderPlayerItems(player);
    if(!player.chatMessage) {
        this.renderPlayerName(idx);
    }
    this.c.restore();
    if(player.chatMessage) {
        this.drawSpeechBubble(position, player.chatMessage);
    }
}

/**
 * Render a player name on each player
 *
 * @param name
 */
WorldRenderer.prototype.renderPlayerName = function(name) {
        this.c.fillStyle = "#5a58df";
        this.c.font = "bold 15px Courier New";
        this.c.fillText(name, -15, -45);
}

/**
 * Render all players in the world
 */
WorldRenderer.prototype.renderPlayers = function() {
    for(var i in this.world.players) {
        var player = this.world.players[i];
        this.renderPlayer(i, player);
    }
}

/**
 * Render all the active player items
 *
 * @param player
 */
WorldRenderer.prototype.renderPlayerItems = function(player) {
    var that = this;
    for(var i in player.items) {
        var item = player.items[i];
        if(!item.active) continue;

        //this.translateCanvasTo(player.body.GetPosition(), player.body.GetAngle());
        var directionOffset = (item.type == Item.TYPE_OZ_FLASK) ? 0 : player.direction * item.pixelWidth;
        var animationOffset = item.handleItemFrameIntervals(player.animationIndex) * item.pixelHeight;
        var xOffset;
        if(player.direction == ClientPlayer.FACING_RIGHT) {
            xOffset = item.rightOffsetX;
        } else {
            xOffset = item.leftOffsetX;
        }
        // If the item is a flask, draw its contents.
        if(item.type == Item.TYPE_OZ_FLASK) {
            drawOxygenFlaskContent(item.content, player.direction);
        }
        this.c.drawImage(
            item.image,
            directionOffset,
            animationOffset,
            item.pixelWidth-1,
            item.pixelHeight-1,
            xOffset,
            item.offsetY,
            item.pixelWidth * 0.41,
            item.pixelHeight * 0.41
        )
    }

    /**
     * Draws the content of a oxygen flask, based on how much is left in the tank.
     *
     * @param value
     * @param direction
     */
    function drawOxygenFlaskContent(value, direction) {
        that.c.beginPath();
        that.c.fillStyle = "#8A2424";
        var xOffset = -30;
        var yOffset = -11;
        var width = 18*0.41;
        var height = 69*0.41;
        if(direction == ClientPlayer.FACING_LEFT) {
            xOffset = 22;
        }
        that.c.rect(xOffset, yOffset, width, height);
        that.c.fill();

        if(value > 0) {
            var newHeight = height * (value / Item.OZ_FLASK_VALUE);
            yOffset += height - newHeight;
            that.c.beginPath();
            that.c.fillStyle = "#71E0FC";
            that.c.rect(xOffset, yOffset, width, newHeight);
            that.c.fill();
        }
    }
}

/**
 * Draw a mountain block
 * @param entity
 * @param position
 * @param angle
 */
WorldRenderer.prototype.drawBody = function(entity, position, angle) {
    this.translateCanvasTo(position, angle);
    this.c.beginPath();

    this.c.fillStyle = "#777";
    if(entity.name == "ice") {
        this.c.fillStyle = "lightblue";
    } else if(entity.name == "snowflake") {
        this.c.fillStyle = "white";
    }
    this.c.rect(
        -entity.width/2,
        -entity.height/2,
        entity.width,
        entity.height
    );
    this.c.fill();
    // Restore the canvas to prev save.
    this.c.restore();
}

WorldRenderer.prototype.drawSnow = function(entity, position, angle) {
    this.translateCanvasTo(position, angle);
    this.c.beginPath();
    this.c.fillStyle = "white";
    this.c.rect(
        -entity.width/2,
        (-entity.height/2)-22,
        entity.width,
        entity.height+10
    );
    this.c.fill();
    this.c.restore();
}

/**
 * Draws a speech bubble at a player position, with some text
 *
 * @param position
 * @param text
 */
WorldRenderer.prototype.drawSpeechBubble = function(position, text) {
    if(!position) return;

    this.c.font = "bold 11px Courier New";
    var textLength = this.c.measureText(text).width;
    var textLengthOffset = (textLength > 40) ? textLength - 30 : 0;
    var boxPosition = {x: (position.x*World.SCALE)-this.camera.offsetX, y: (position.y*World.SCALE)-this.camera.offsetY - 80};
    this.c.fillStyle = "rgba(255, 255, 255, 0.85)";
    this.c.beginPath();
    this.c.moveTo(boxPosition.x, boxPosition.y);
    this.c.lineTo(boxPosition.x +50 + textLengthOffset, boxPosition.y);
    this.c.lineTo(boxPosition.x +50 + textLengthOffset, boxPosition.y + 30);
    this.c.lineTo(boxPosition.x +30, boxPosition.y + 30);
    this.c.lineTo(boxPosition.x +15, boxPosition.y + 50);
    this.c.lineTo(boxPosition.x +15, boxPosition.y + 30);
    this.c.lineTo(boxPosition.x, boxPosition.y + 30);
    this.c.closePath();
    this.c.fill();
    this.c.fillStyle = "Black";
    this.c.fillText(text, boxPosition.x + 7, boxPosition.y + 18);
}