function UIRenderer(context, camera, player, chat) {
    this.c = context;
    this.camera = camera;
    this.player = player;
    this.chat = chat;
}

UIRenderer.prototype = new Renderer();
UIRenderer.constructor = Renderer;

UIRenderer.ITEM_BAR_X = 20;
UIRenderer.ITEM_BAR_Y = 20;
UIRenderer.ICON_SIZE = 31;
/**
 * Draws life and oxygen bars for player
 */
UIRenderer.prototype.drawBars = function() {
    // Save the canvas state
    var that = this;
    this.c.save();
    this.c.beginPath();
    var start = 250;
    var healthWidth = (this.player.health/Player.MAX_HEALTH) * 150;

    drawBar(start, "#de4f4f", healthWidth);
    drawBar(start+healthWidth, "#4f78de", (this.player.oxygen/Player.MAX_OXYGEN) * 110);
    this.c.restore();

    /**
     * Draws a health bar with a color, start position and value
     * @param start
     * @param color
     * @param width
     */
    function drawBar(start, color, width) {
        that.c.beginPath();
        that.c.fillStyle = color;
        that.c.rect(
            start, 20, width,15
        );
        that.c.fill();
    }
}

UIRenderer.prototype.render = function() {
    this.renderChat();
    this.renderItemBar();
    this.drawBars();
}

/**
 * Renders the chat using some "private" chat
 * related helper methods
 *
 */
UIRenderer.prototype.renderChat = function() {
    var position = {
        x: 35,
        y:this.camera.height-(Chat.TEXT_FIELD_HEIGHT * 1.5)
    };

    var that = this;
    var drawFieldText = function() {
        that.c.fillStyle = "white";
        that.c.font = "15px Courier New";
        that.c.fillText(that.chat.text, position.x+10, position.y+(Chat.TEXT_FIELD_HEIGHT)-7);
        var textLength = that.c.measureText(that.chat.text);
        drawBlinker(position.x+11+textLength.width, position.y+4);
    }

    var drawBlinker = function(x, y) {
        if(that.chat.showBlinker) {
            that.drawRoundRect(x, y, 1, 16, 0, "white");
        }
    }

    if(this.chat.isOpen) {
        this.drawRoundRect(position.x, position.y, Chat.TEXT_FIELD_WIDTH, Chat.TEXT_FIELD_HEIGHT, 0, "rgba(0, 0, 0, 0.7)");
        drawFieldText();
    }
}

/**
 * Renders the item bar and icons for current player items.
 */
UIRenderer.prototype.renderItemBar = function() {
    this.c.save();
    this.c.drawImage(Assets.itemBar, UIRenderer.ITEM_BAR_X, UIRenderer.ITEM_BAR_Y);
    var that = this;
    renderBarItemIcons(this.player.items);
    this.c.restore();

    function renderBarItemIcons(items) {
        var x = UIRenderer.ITEM_BAR_X;
        var boxNumber = 1;
        for(var i in items) {
            var item = items[i];
            var activeOffset = ((item.active) ? 0 : 1) * UIRenderer.ICON_SIZE;
            var itemOffset = item.type * UIRenderer.ICON_SIZE;
            that.c.drawImage(
                Assets.icons,
                activeOffset,
                itemOffset,
                UIRenderer.ICON_SIZE,
                UIRenderer.ICON_SIZE,
                x+5,
                UIRenderer.ITEM_BAR_Y+5,
                UIRenderer.ICON_SIZE,
                UIRenderer.ICON_SIZE
            );
            that.c.font = "bold 17px Courier New";
            that.c.fillStyle = "white";
            that.c.fillText(boxNumber++, x+5, UIRenderer.ITEM_BAR_Y+16);
            x += UIRenderer.ICON_SIZE+11;
        }
    }
}