function Renderer() {
    this.c = null;
    this.camera = null;
}

/**
 * Translate the canvas to a position and angle
 * using camera offset.
 *
 * @param position
 * @param angle
 */
Renderer.prototype.translateCanvasTo = function(position, angle) {
    // Save the canvas state
    this.c.save();
    // translate and rotate canvas
    this.c.translate((position.x*World.SCALE)-this.camera.offsetX, (position.y*World.SCALE)-this.camera.offsetY);
    if(angle != 0) {
        this.c.rotate(angle);
    }
}
/**
 * ABSTRACT RENDER FUNCTION
 * Implementation should contain all render calls needed
 */
Renderer.prototype.render = function() { }

/**
 * Draws a rect with optional rounded corners.
 *
 * @param x
 * @param y
 * @param w
 * @param h
 * @param cr
 * @param color
 */
Renderer.prototype.drawRoundRect = function(x, y, w, h, cr, color) {
    this.c.fillStyle = color;
    this.c.beginPath();
    this.c.moveTo(x + w / 2, y);
    this.c.arcTo(x + w, y, x + w, y + h, cr);
    this.c.arcTo(x + w, y + h, x, y + h, cr);
    this.c.arcTo(x, y + h, x, y, cr);
    this.c.arcTo(x, y, x + w, y, cr);
    this.c.closePath();
    this.c.fill();
}