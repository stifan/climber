function Client() {
    this.socket = null;
    this.name = "";
    this.game = null;
    var socketLink = "ws://127.0.0.1:3333";
    //socketLink = "ws://boiling-spire-2220.herokuapp.com";
    var that = this;
    if(checkSocketSupport()) {
        setupSocket();
        this.game = new ClientGame(this.socket);
    }
    /**
     * Tries to establish a connections using a context based
     * WebSocket object
     *
     * @returns {boolean}
     */
    function checkSocketSupport() {
        try {
            if(typeof MozWebSocket !== "undefined") {
                that.socket = new MozWebSocket(socketLink);
                return true;
            } else if(typeof WebSocket !== "undefined") {
                that.socket = new WebSocket(socketLink);
                return true;
            } else {
                return false;
            }
        } catch(err) {
            return false;
        }
    }

    /**
     * Sets up the socket
     */
    function setupSocket() {
        that.socket.onerror = function(err) {
            console.log("websocket error");
        }
        // Stop the game when the connection is closed
        that.socket.onclose = function(err) {
            that.game.stop();
            $("#serverConnection").text("Connected: No");
        }
        // Send handshake message when opening connection
        that.socket.onopen = function() {
            $("#serverConnection").text("Connected: Yes");
            that.socket.send(JSON.stringify({
                t: "hi",
                d: that.name
            }));
        }
        // Validate the received message and pass it to the game if ok.
        that.socket.onmessage = function(e) {
            var optimizedMessage;
            // Check message format
            try {
                optimizedMessage = JSON.parse(e.data);
            } catch(err) {
                console.log("Error in message format");
                return;
            }
            //console.log(e.data);
            /**
            data.c = connection.chatMessage;
            data.p = connection.player.getPlayerData();
            data.a = connection.appliedInputs;
             */
            var players = {};

            for(var id in optimizedMessage.p) {
                var optimizedplayer = optimizedMessage.p[id];
                var player = {}
                if(optimizedplayer.c) {
                    player.chatMessage = optimizedplayer.c;
                }
                player.player = {}
                player.player.angle = optimizedplayer.p.a;
                player.player.position = optimizedplayer.p.p;
                player.player.velocity = optimizedplayer.p.v;
                player.player.h = optimizedplayer.p.h;
                player.player.o = optimizedplayer.p.o;
                player.player.i = optimizedplayer.p.i;
                player.ai = optimizedplayer.a;
                players[id] = player;
            }

            for(var id in optimizedMessage.m) {
                var item = optimizedMessage.m[id];
                item.p.x = item.p.x/1000;
                item.p.y = item.p.y/1000;
            }
            var message = {
                myIndex: optimizedMessage.i,
                players: players,
                lpi: optimizedMessage.l,
                removes: optimizedMessage.r,
                items: optimizedMessage.m,
                time: optimizedMessage.t
            }
            //console.log(JSON.stringify(optimizedMessage));
            // revert all players

            that.game.handleClientMessage(message);
        }
    }
}
