function Keys() {
    this.keyCodes = {
        32: {down: false, action: function(player, deltaTime) {return player.jump();}}, // space
        39: {down: false, action: function(player, deltaTime) {return player.decideMovement(true, deltaTime);}}, // right arrow
        37: {down: false, action: function(player, deltaTime) {return player.decideMovement(false, deltaTime);}}, // left arrow
        40: {down: false, action: function() {}}, // arrow down
        38: {down: false, action: function(player, deltaTime) {return player.getUp();}} // arrow up
    }
}

Keys.ARROW_LEFT = 37;
Keys.ARROW_RIGHT = 39;


