var Player = function Player() {
    this.health = Player.MAX_HEALTH;
    this.oxygen = Player.MAX_OXYGEN;
    this.breatheNormal = true;

    this.jumpTimeOut = 0;

    this.fixtureUnderFoot = null;
    this.fixtureOnRight = null;
    this.fixtureOnLeft = null;

    this.chatMessage = null;

    this.items = {};
}

Player.MAX_VEL = 5;
Player.VEL_INC = 10;
Player.JUMP_TIMOUT_VAL = 20;
Player.GET_UP_TIMEOUT_VAL = 50;
Player.MAX_OXYGEN = 750;
Player.MAX_HEALTH = 1000;

/**
 * Abstract Player update method, implement at will
 * @param deltaTime
 */
Player.prototype.update = function(deltaTime) { }

/**
 * Will handle the fixed rotation of the player
 * will set it to fixed when he is standing up
 * making sure he does not fall over when walking.
 */
Player.prototype.handleFixedRotation = function() {
    if(!this.fixtureUnderFoot && this.body.IsFixedRotation()) {
        this.body.SetFixedRotation(false);
    } else if(this.fixtureUnderFoot && !this.body.IsFixedRotation()) {
        this.body.SetFixedRotation(true);
        this.body.SetAngularVelocity(0);
        this.body.SetAngle(0);
    }
}

/**
 * Based on what the player is currently touching
 * this method will decide what movement the player will do
 * @param goRight
 * @returns {*}
 */
Player.prototype.decideMovement = function(goRight, deltaTime) {
    // Make sure that any of the sensors are triggered
    if(!this.fixtureUnderFoot && !this.fixtureOnLeft && !this.fixtureOnRight) {
        return null;
    }

    // Check if the foot sensor is hit, and skip crawling
    if(this.fixtureUnderFoot) {
        return this.walk(goRight, deltaTime);
    }
}

/**
 * ABSTRACT FUNCTION MUST BE IMPLEMENTED IN SUB CLASS
 *
 * @param goRight
 * @param deltaTime
 * @returns {null}
 */
Player.prototype.walk = function(goRight, deltaTime) { return null; }

/**
 * Make sure the player is allowed to walk before walking!
 * Set the body to be awake to enable box2d calculations
 *
 * @returns {boolean}
 */
Player.prototype.isAllowedToWalk = function() {
     if(this.body.GetLinearVelocity().y < -0.01 || this.body.GetLinearVelocity().y > 0.01) {
         return false;
     }
     this.body.SetAwake(true);
     return true;
}

/**
 * Stops the player by applying 0 velocity.
 *
 * @returns {boolean}
 */
Player.prototype.stop = function() {
    if(!this.fixtureUnderFoot) return false;
    var velocity = this.body.GetLinearVelocity();
    velocity.x = 0;
    this.body.SetLinearVelocity(velocity);
    return true;
}

/**
 * Implement in sub object,
 * should return whether or not a player
 * has active ice shoes.
 *
 * @returns {boolean}
 */
Player.prototype.hasActiveIceShoes = function() { return false; }

/**
 * ABSTRACT JUMP METHOD IMPLEMENT IN SUB OBJECT
 */
Player.prototype.jump = function() { }

/**
 * Calculate the jump force
 *
 * Based on player oxygen level
 */
Player.prototype.calcJump = function() {
    var force = 12;
    if(this.oxygen < 20) {
        force /= 1.5;
    }
    return this.calcForceVector(0, -force);
}

/**
 * Check if the player is allowed to jump
 * @returns {boolean}
 */
Player.prototype.canJump = function() {
    if(this.jumpTimeOut > 0 || !this.fixtureUnderFoot || this.body.GetLinearVelocity().y < -0.01) {
        return false;
    }
    return true;
}

/**
 * Calculate a vector for player mass and a given force
 * @returns {Box2D.Common.Math.b2Vec2}
 */
Player.prototype.calcForceVector = function(x, y) {
    return new this.b2Vec2(this.mass * x, this.mass * y);
}

/**
 * ABSTRACT GET UP METHOD IMPLEMENT IN SUB OBJECT
 */
Player.prototype.getUp = function() { }

/**
 * Checks if the player is allowed to get up.
 * For that he need to be still and not having a fixture on his foot
 * and jump timeout should be 0
 *
 * @returns {boolean}
 */
Player.prototype.canIGetUp = function() {
    return (!this.fixtureUnderFoot && this.jumpTimeOut < 1 && this.body.GetLinearVelocity().x == 0 && this.body.GetLinearVelocity().y == 0)
}

Player.prototype.applyInput = function(input) { }

try {
    module.exports = Player;
} catch(e) {
    // context does not know about the exports.
}