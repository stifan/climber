try {
    var MathHelper = require("./MathHelper.js");
    var Item = require("./Item.js");
} catch(e) { }

function World() {
    this.b2dworld = null;
    this.entities = [
        {type:"block", name:"wall", x:15,y:World.HEIGHT/2,angle:0,width:30,height:World.HEIGHT, isStatic: true}, // Left wall
        {type:"block", name:"wall", x:World.WIDTH-15,y:World.HEIGHT/2,angle:0,width:30,height:World.HEIGHT, isStatic: true}, // Right wall
        {type:"block", name:"bedrock", x:World.WIDTH/2,y:World.HEIGHT-15,angle:0,width:World.WIDTH,height:30, isStatic: true} // Bottom
        ];
    this.players = {};
    this.items = [];
    this.queuedItemsForDeletion = {};
    // box2d vars
    this.b2ContactListener = null;
    this.b2WorldManifold = null;
    this.b2Vec2 = null;
    this.b2BodyDef = null;
    this.b2Body = null;
    this.b2FixtureDef = null;
    this.b2Fixture = null;
    this.b2World = null;
    this.b2PolygonShape = null;
    this.b2CircleShape = null;
    this.b2DebugDraw = null;
    this.contactListener = null;

    this.definitions = { // Definitions object that defines the types of material in the world
        "rock": {
            density: 3.0,
            friction: 2,
            restitution: 0.1
        },
        "bedrock": {
            density: 3.0,
            friction: 2,
            restitution: 0.1
        },
        "ice": {
            density: 2.5,
            friction: 0.01,
            restitution: 0.1
        },
        "snow": {
            density: 3.0,
            friction: 2,
            restitution: 0.2
        },
        "snowflake" : {
            density: 0.5,
            friction: 0.1,
            restitution: 0.1
        },
        "wall": {
            density: 3.0,
            friction: 1,
            restitution: 0.1
        },
        "player": {
            density: 1,
            friction: 1,
            restitution: 0.1
        },
        "item": {
            density: 1,
            friction: 1,
            restitution: 0.2
        }
    }
}

/**
 * Defining static World vars
 */
World.SCALE = 30;
World.GRAVITY = 16;
World.WIDTH = 3000;
World.HEIGHT = 1500;

// Hexes for collision filtering
World.COLLIDE_PLAYER_HEX = 0x0001;
World.COLLIDE_BLOCK_HEX = 0x0002;
World.COLLIDE_ITEM_HEX = 0x0003;
World.COLLIDE_FLAKE_HEX = 0x0004;

/**
 * Initialize the World, start trying to init box2d
 * If that fails, dont do anything else.
 */
World.prototype.init = function() {
    if(this.initBox2d()) {
        var gravity = new this.b2Vec2(0, World.GRAVITY);
        var allowSleep = true;
        // Create the box2d world.
        this.b2dworld = new this.b2World(gravity, allowSleep);
        // initialize a new contact listener
        this.contactListener = new this.b2ContactListener;
        // Create the static world for the moment.
        this.createWorld();
        // call abstract setup method
        this.setup();
        // Set the contact listeners
        this.setupContactListeners();
        this.b2dworld.SetContactListener(this.contactListener);
        return true;
    } else {
        console.log("Could not initialize box2d do not continue creating the world!");
        return false;
    }
}

/**
 * IMPLEMENT IF NEEDED IN SUB CLASS
 *
 * for sub class specific setup
 */
World.prototype.setup = function() {}

/**
 * Abstract method implement if needed
 *
 * Is called when a player hits a snow block
 */
World.prototype.hitSnow = function(position) {}
/**
 * IMPLEMENT THIS IN SUB CLASS
 *
 * Purpose is to setup the box2d vars
 *
 * @returns {boolean}
 */
World.prototype.initBox2d = function() { console.log("init b2"); return false; }

/**
 * Will update all non box2d related vars
 *
 * @param deltaTime
 * @returns {boolean}
 */
World.prototype.update = function(deltaTime) {
    for(var id in this.players) {
        this.players[id].update(deltaTime);
    }
}

/**
 * Step the world using "abstract" update method
 * and stepping the box2d world.
 *
 * @param deltaTime
 */
World.prototype.step = function(deltaTime) {
    this.update(deltaTime);
    // velocity iterations = 8
    // position iterations = 3
    this.b2dworld.Step(deltaTime, 8, 3);
    this.b2dworld.ClearForces();
    for(var i in this.queuedItemsForDeletion) {
        this.removeBody(this.queuedItemsForDeletion[i]);
        delete this.items[i];
    }
}

/**
 * Creates the blocks for the world and applies them
 * to the box2d world.
 */
World.prototype.createWorld = function() {
    var yDistance = 120;
    var xDistance = 300;
    var yStart = World.HEIGHT - 20;
    var numOfLevels = (yStart / yDistance);
    var wallLength = Math.sqrt(Math.pow((World.WIDTH/2),2) + Math.pow(World.HEIGHT, 2));
    var topAngle = Math.asin((World.WIDTH/2) / wallLength);

    for(var i = 1; i < numOfLevels; i++) {
        var y = yStart - (i * yDistance);
        // calculate the width of the current level based on how high we are on the mountain
        var levelLength = (y * Math.tan(topAngle)) * 2;
        // calculate num of horizontal blocks on this level remove decimals
        var numOfBlocks = levelLength / xDistance | 0;
        var leftOnMountain = levelLength - (numOfBlocks*xDistance);
        var xBuffer = (leftOnMountain) / (numOfBlocks+1);
        var x = (World.WIDTH - levelLength) / 2 - 150;
        for(var j = 0; j < numOfBlocks; j++) {
            x += xDistance + xBuffer;
            var type = "rock";
            if(i > 5 && i < 8) {
                type = "ice";
             } else if(i > 7) {
                type = "snow";
             }
            this.entities.push(
                {type:"block", name:type, x:x, y:y, angle: 0, width:200, height:20, isStatic: true}
            );
        }
    }
    for(var i = 0; i < this.entities.length; i++) {
        this.createBlock(this.entities[i]);
    }
}

/**
 * Tries to create a block based on a entity name
 * and a definition of the block type.
 *
 * @param entity
 *
 */
World.prototype.createBlock = function(entity) {
    var definition = this.definitions[entity.name];
    if(!definition) {
        console.log("undefined definition name ", entity.name);
        return;
    }
    switch(entity.type) {
        case "block":
            // create a rectangle
            return this.createRectangle(entity, definition);
            break;

    }
}

/**
 * Create a rectangle in the box2d world
 *
 * @param entity
 * @param definition
 * @returns {body of the rectangle}
 */
World.prototype.createRectangle = function(entity,definition){
    var bodyDef = new this.b2BodyDef;
    if(entity.isStatic){
        bodyDef.type = this.b2Body.b2_staticBody;
    } else {
        bodyDef.type = this.b2Body.b2_dynamicBody;
    }

    bodyDef.position.x = entity.x/World.SCALE;
    bodyDef.position.y = entity.y/World.SCALE;
    if (entity.angle) {
        bodyDef.angle = Math.PI*entity.angle/180;
    }

    var fixtureDef = new this.b2FixtureDef;
    fixtureDef.density = definition.density;
    fixtureDef.friction = definition.friction;
    fixtureDef.restitution = definition.restitution;

    fixtureDef.shape = new this.b2PolygonShape;
    fixtureDef.shape.SetAsBox(entity.width/2/World.SCALE,entity.height/2/World.SCALE);
    if(entity.name == "snowflake") {
        fixtureDef.filter.categoryBits = World.COLLIDE_FLAKE_HEX;
        fixtureDef.filter.maskBits = World.COLLIDE_BLOCK_HEX;
    } else {
        fixtureDef.filter.categoryBits = World.COLLIDE_BLOCK_HEX;
        fixtureDef.filter.maskBits = World.COLLIDE_PLAYER_HEX | World.COLLIDE_FLAKE_HEX;
    }

    var body = this.b2dworld.CreateBody(bodyDef);
    body.SetUserData(entity);
    body.CreateFixture(fixtureDef);

    return body;
}

/**
 * Creates a player body and adds it to the game world
 * @returns {box2d body}
 */
World.prototype.createPlayer = function(id, position) {
    var bodyDef = new this.b2BodyDef;
    bodyDef.type = this.b2Body.b2_dynamicBody;
    bodyDef.fixedRotation = true;

    var polygonShape = new this.b2PolygonShape;
    polygonShape.SetAsBox(20/World.SCALE,37/World.SCALE);

    var fixtureDef = new this.b2FixtureDef;
    fixtureDef.shape = polygonShape;
    fixtureDef.density = this.definitions["player"].density;
    fixtureDef.friction = this.definitions["player"].friction;

    fixtureDef.filter.categoryBits = World.COLLIDE_PLAYER_HEX;
    fixtureDef.filter.maskBits = World.COLLIDE_BLOCK_HEX | World.COLLIDE_ITEM_HEX;

    bodyDef.position.Set(position.x, position.y);
    var body = this.b2dworld.CreateBody(bodyDef);
    body.SetUserData({id: id, health: 1000});
    body.CreateFixture(fixtureDef);

    // add foot sensor fixture
    polygonShape.SetAsOrientedBox(6/World.SCALE, 1/World.SCALE, new this.b2Vec2(0/World.SCALE,40/World.SCALE), 0);
    fixtureDef.isSensor = true;
    var footSensorFixture = body.CreateFixture(fixtureDef);
    footSensorFixture.SetUserData({id: id, sId:3});

    // add front sensor fixture
    polygonShape.SetAsOrientedBox(2/World.SCALE, 24/World.SCALE, new this.b2Vec2(20/World.SCALE, 0/World.SCALE), 0);
    var frontSensorFixture = body.CreateFixture(fixtureDef);
    frontSensorFixture.SetUserData({id: id, sId:2});

    // add back sensor fixture
    polygonShape.SetAsOrientedBox(2/World.SCALE, 24/World.SCALE, new this.b2Vec2(-20/World.SCALE, 0/World.SCALE), 0);
    var leftSensorFixture = body.CreateFixture(fixtureDef);
    leftSensorFixture.SetUserData({id: id, sId:1});

    return body;
}

/**
 * Removes a body from the world
 * @param body
 */
World.prototype.removeBody = function(body) {
    this.b2dworld.DestroyBody(body);
}

/**
 * Sets up contact listeners for all players
 * And one way collision.
 */
World.prototype.setupContactListeners = function() {
    var that = this;
    this.contactListener.BeginContact = function(contact) {
        that.setupSensors(true, contact);
        that.setupOneWayCollision(contact);
    }
    this.contactListener.EndContact = function(contact) {
        that.setupSensors(false, contact);
        contact.SetEnabled(true);
    }
}

/**
 * Setup listeners when two objects starts to collide.
 *
 * If any of the two are a player, and the contact is between
 * a sensor, set the matching sensor contact.
 *
 * And reset the player fixture on ending a contact
 *
 * @param contact
 */
World.prototype.setupSensors = function(begin, contact) {
    var entityA = contact.GetFixtureA();
    var entityB = contact.GetFixtureB();

    var bodyFixture;
    var worldFixture;

    if(entityA.GetUserData()) {
        bodyFixture = entityA;
        worldFixture = entityB;
    } else if(entityB.GetUserData()) {
        bodyFixture = entityB;
        worldFixture = entityA;
    }

    // Return if there is no body fixture, as it is not important
    if(!bodyFixture) {
        return;
    }

    // If it is a end contact, set the fixture contact to null,
    // because the player is not touching that contact anymore.
    var worldBodyData = worldFixture.GetBody().GetUserData();
    var fixtureUserData = bodyFixture.GetUserData();

    if(!begin) {
        if(worldBodyData.name == "ice") {
            worldFixture.SetFriction(this.definitions[worldBodyData.name].friction);
        }
        worldFixture = null;
    }

    if(fixtureUserData.sId == 3) { // Foot sensor
        if(begin && worldBodyData.name == "ice" && this.players[fixtureUserData.id].hasActiveIceShoes()) {
            worldFixture.SetFriction(3);
        }
        this.players[fixtureUserData.id].fixtureUnderFoot = worldFixture;
        return;
    }
    if(fixtureUserData.sId == 2) { // right sensor
        this.players[fixtureUserData.id].fixtureOnRight = worldFixture;
        return;
    }
    if(fixtureUserData.sId == 1) { // left sensor
        this.players[fixtureUserData.id].fixtureOnLeft = worldFixture;
        return;
    }
}

/**
 * Setup one way collisions between player and boxes
 * Doing so by checking the direction of the colliding points.
 *
 * @param contact
 */
World.prototype.setupOneWayCollision = function(contact) {
    var entityA = contact.GetFixtureA().GetBody();
    var entityB = contact.GetFixtureB().GetBody();
    var worldBody;
    var otherBody;
    var worldFixture;

    if(entityA.GetUserData().hasOwnProperty("type")) {
        worldBody = entityA;
        otherBody = entityB;
        worldFixture = contact.GetFixtureA();
    } else if(entityB.GetUserData().hasOwnProperty("type")) {
        worldBody = entityB;
        otherBody = entityA;
        worldFixture = contact.GetFixtureB();
    }
    if(!worldBody) {
        return;
    }

    var worldBodyName = worldBody.GetUserData().name;
    // Handle item collision
    if(worldBodyName == "item") {
        var itemData = worldBody.GetUserData();
        var playerData = otherBody.GetUserData();
        contact.SetEnabled(false);
        if(
            MathHelper.objectSize(this.players[playerData.id].items) > 4 ||
            this.players[playerData.id].hasItem(this.items[itemData.id].type)
        ) {
            var filter = worldFixture.GetFilterData();
            filter.categoryBits = World.COLLIDE_ITEM_HEX;
            filter.maskBits = 0;
            worldFixture.SetFilterData(filter);
            return;
        }
        var item = this.items[itemData.id];
        var value = null;
        if(item.type == Item.TYPE_OZ_FLASK) {
            value = Item.OZ_FLASK_VALUE;
        }
        this.players[playerData.id].items[itemData.id] = {t: item.type, v: value, a: false};
        this.queuedItemsForDeletion[itemData.id] = (item.body);
        return;
    } else if(worldBodyName == "snowflake") {
        return;
    }


    var numPoints = contact.GetManifold().m_pointCount;
    var worldManifold = new this.b2WorldManifold;
    var playerPosition = otherBody.GetPosition();
    contact.GetWorldManifold(worldManifold);
    // check if contact points are moving downward
    for(var i = 0; i < numPoints; i++) {
        // Using .x here due to a bug in box2d where each points is inside the x value of the point :|
        //console.log();
        var pointVelOther = new this.b2Vec2(otherBody.GetLinearVelocityFromWorldPoint(worldManifold.m_points[i])).x;
        var pointVelPlatform = new this.b2Vec2(worldBody.GetLinearVelocityFromWorldPoint(worldManifold.m_points[i])).x;
        var relativeVel = worldBody.GetLocalVector(MathHelper.subtractVectors(pointVelOther, pointVelPlatform));
        // Body always jumps a bit so if it is very little
        // He is probably walking into a box, then enable collision
        if(relativeVel.y > -0.1) {
            return;
        }
        if(worldBody.GetUserData().name == "snow") {
            var position = worldManifold.m_points[i];
            var offset = 0.2;
            if(playerPosition.x > position.x) {
                position.x += offset;
            } else {
                position.x -= offset;
            }
            this.hitSnow(position);
        }
    }
    contact.SetEnabled(false);
}

try {
    module.exports = World;
} catch(e) {
    // context does not know about the exports.
}