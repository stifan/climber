function Item(type) {
    var definitions = {
        0: {
            name: "Oxygen Mask",
            type: Item.TYPE_OZ_MASK,
            image: Assets.oxygenMask,
            rightOffsetX: -20,
            leftOffsetX: -29,
            offsetY: -35,
            pixelWidth: 121,
            pixelHeight: 88,
            value: null,
            frameIntervals: [
                [3, 4],
                [11, 12]
            ]
        },
        1: {
            name: "Ice shoes",
            type: Item.TYPE_ICE_SHOE,
            image: Assets.iceShoes,
            rightOffsetX: -26,
            leftOffsetX: -26,
            offsetY: 35,
            pixelWidth: 128,
            pixelHeight: 11,
            value: null,
            frameIntervals: []
        },
        2: {
            name: "Oxygen Flask",
            type: Item.TYPE_OZ_FLASK,
            image: Assets.oxygenFlask,
            rightOffsetX: -33,
            leftOffsetX: 19,
            offsetY: -25,
            pixelWidth: 32,
            pixelHeight: 113,
            content: Item.OZ_FLASK_VALUE,
            frameIntervals: [
                [3, 4],
                [11, 12]
            ]
        }

    }

    var itemDefinition = definitions[type];
    if(!itemDefinition) return null;

    this.name = itemDefinition.name;
    this.type = itemDefinition.type;
    this.active = false;
    this.rightOffsetX = itemDefinition.rightOffsetX;
    this.leftOffsetX = itemDefinition.leftOffsetX;
    this.offsetY = itemDefinition.offsetY;
    this.image = itemDefinition.image;
    this.pixelWidth = itemDefinition.pixelWidth;
    this.pixelHeight = itemDefinition.pixelHeight;
    this.frameIntervals = itemDefinition.frameIntervals;
    this.content = itemDefinition.content;
}

Item.TYPE_OZ_MASK = 0;
Item.TYPE_ICE_SHOE = 1;
Item.TYPE_OZ_FLASK = 2;

Item.OZ_FLASK_VALUE = 2000;

/**
 * Handle item specific frames, based on the frame
 * intervals array in the item definition
 *
 * If one of the intervals returns true, the functions returns with one
 * if none of the intervals are true, returns 0
 *
 * This will only work with items containing two frames, or items
 * matching the amount of running frames.
 *
 * @param animationIndex
 * @returns {Number}
 */
Item.prototype.handleItemFrameIntervals = function(animationIndex) {
    // If there is no rules for animation intervals, return the animation index
    if(this.frameIntervals.length < 1) return (animationIndex > 15) ? 11 : animationIndex;
    for(var i in this.frameIntervals) {
        var interval = this.frameIntervals[i];
        if(animationIndex >= interval[0] && animationIndex <= interval[1]) {
            return 1;
        }
    }
    return 0;
}

try {
    module.exports = Item;
} catch(e) {
    // context does not know about the exports.
}
