var Game = function Game() {
    this.world = null;
}

/**
 * Tries to initialize the game, and create game world.
 */
Game.prototype.init = function() {
    this.createWorld();
}
/**
 * IMPLEMENT THIS IN SUB CLASS
 * Instantiates a new world
 * @returns {boolean}
 */
Game.prototype.createWorld = function() { return false; }

try {
    module.exports = Game;
} catch(e) {
    // context does not know about the exports.
}
