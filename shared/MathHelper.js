function MathHelper() { }

/**
 * Substract two vectors and return the new
 *
 * @param vec1
 * @param vec2
 * @returns {*}
 */
MathHelper.subtractVectors = function(vec1, vec2) {
    vec1.x -= vec2.x;
    vec1.y -= vec2.y;
    return vec1;
}

MathHelper.DEG_TO_RAD = (Math.PI / 180);
MathHelper.RAD_TO_DEG = (180 / Math.PI);

/**
 * Counts the size of an object by counting its properties
 * @param obj
 * @returns {number}
 */
MathHelper.objectSize = function(obj) {
    var size = 0;
    for(var key in obj) {
        if(obj.hasOwnProperty(key)) size++;
    }
    return size;
}

try {
    module.exports = MathHelper;
} catch (e) {
    // In client, no node context
}