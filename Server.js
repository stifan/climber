function Server() {
    // Required NodeJS objects
    this.http = null;
    this.websocketServerObject = null;
    this.Game = null;
    this.Player = null;
    this.MathHelper = null;
    this.lastGameSend = Date.now();
    var that = this;
    loadRequires();

    // Define current process port or fallback to default port
    this.port = Number(process.env.PORT || 3333);

    // The two servers
    this.httpServer = null;
    this.socketServer = null;

    // Setup the game
    this.game = new this.Game();

    // Current server connections and game instance
    this.connections = {};
    this.queuedDisconnects = [];
    this.lastProcessedInput = [];

    // Server loop properties
    this.frame = 0;
    this.frames = 0;
    this.startTime = 0;

    /**
     * Loads the required node libraries for server
     */
    function loadRequires() {
        that.system = require("util");
        that.http = require("http");
        that.websocketServerObject = require("websocket").server;
        that.os = require('os-utils');
        that.Game = require("./lib/ServerGame.js");
        that.Player = require("./lib/ServerPlayer.js");
        that.MathHelper = require("./shared/MathHelper.js");
    }

    this.createHttpServer();
    this.createSocketServer();
    this.serverLoop();
}

// Static properties
Server.MAX_CONNECTIONS = 11;
Server.FRAMES_PR_GAME_STATE_TRANSMISSION = 5;
Server.CONNECTION_TYPE_PLAYER = 0;
Server.CONNECTION_TYPE_CHART = 1;
Server.GAME_FPS = 60;

/**
 * Creates an http server that listens on a specific port
 * and responds with a status code 200.
 *
 * This server will be used as a base for the web socket server, and
 * have no real purpose besides that.
 */
Server.prototype.createHttpServer = function() {
    var that = this;
    this.httpServer = this.http.createServer(function(request, response) {
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end();
    }).listen(this.port, function() {
        that.system.log("Listening for connections on port " + that.port);
    });
}

/**
 * Sets up a socket server based on the previous http server
 *
 * And set up listeners for client messages.
 */
Server.prototype.createSocketServer = function() {
    this.socketServer = new this.websocketServerObject({
        httpServer: this.httpServer,
        closeTimeout: 2000
    });
    var that = this;
    this.socketServer.on("request", function(request) {
        if(!that.isRequestAllowed(request)) return;
        that.setupConnection(request);
    });
}

/**
 * Validates if a new connections is allowed based on how many
 * is currently online.
 *
 * @param request
 * @returns {boolean}
 */
Server.prototype.isRequestAllowed = function(request) {
    if(this.MathHelper.objectSize(this.connections) >= Server.MAX_CONNECTIONS) {
        request.reject();
        return false;
    }
    return true;
}

/**
 * Sets up a new connection with on listeners for "message" and "close"
 *
 * @param request
 */
Server.prototype.setupConnection = function(request) {
    var connection = request.accept(null, request.origin);
    connection.ip = request.remoteAddress;
    var id;
    do {
        id = Math.floor(Math.random() * 1000);
    } while(id in this.connections);
    connection.id = id;
    this.connections[connection.id] = connection;

    var that = this;
    // Handle connection message
    connection.on("message", function(message) {
        if(message.type == "utf8") {
            var message = that.validateMessage(connection.id, message.utf8Data);
            if(message) {
                that.handleClientMessage(connection.id, message);
            }
        }
    });

    // handle connection close
    connection.on("close", function() {
        that.handleClientClosure(connection.id);
    });

    this.system.log(connection.id + " Logged in " + connection.ip + "; currently " + this.MathHelper.objectSize(this.connections) + " users.");
}
/**
 * Will take a client message and call what is needed based on
 * the message type.
 *
 * @param id
 * @param message
 */
Server.prototype.handleClientMessage = function(id, message) {
    var connection = this.connections[id];
    // Switch on the message type
    switch(message.t) {
        case "hi": // initial handshake message, create a new player
            this.createNewPlayer(connection);
            break;
        case "chart": // chart is connected
            connection.type = Server.CONNECTION_TYPE_CHART;
            this.system.log("chart connected");
            break;
        case "i": // input from connection
            this.handleClientInput(connection, message);
            break;
        case "cm": // chat message
            // break if the connection does not have a player
            if(!connection.player) break;
            connection.chatMessage = message.d;
            connection.chatCountDown = 40;
            break;
        case "ti": // toggle item
            if(!connection.player) break;
            connection.player.toggleItem(message.d);
            break;
    }
}

/**
 * Creates a new player and adds it the box2d world,
 * then saves a reference to that player in the connections
 * object.
 *
 * @param connection
 */
Server.prototype.createNewPlayer = function(connection) {
    // make sure that the connection does not have a player already
    if(connection.player) return;
    var body = this.game.addPlayerToWorld(connection.id);
    connection.player = new this.Player(body);
    connection.player.index = connection.id;
    connection.player.b2Vec2 = this.game.world.b2Vec2;
    connection.type = Server.CONNECTION_TYPE_PLAYER;
    connection.appliedInputs = [];
    // Add player to the world players array
    this.game.world.players[connection.id] = connection.player;
    this.system.log(connection.id + " just created a new player");
}

/**
 * Handles a client input by applying that input to the player
 * and storing the input for later server reconciliation.
 *
 * @param connection
 * @param message
 */
Server.prototype.handleClientInput = function(connection, message) {
    // Make sure the connection has a player
    if(!connection.player) return;
    // Apply the player input
    connection.player.applyInput(message.d);
    // push the applied input to the applied inputs array
    connection.appliedInputs.push(message.d);
    // apply the list processed input serial number for the current connection
    this.lastProcessedInput[connection.id] = message.s;
}

/**
 * Validates a client message, by checking if message sender is a current connection
 * that the message is JSON parsable, and the message is in the right format
 *
 * @param id
 * @param message
 * @returns {*}
 */
Server.prototype.validateMessage = function(id, message) {
    // Make sure the sender id exists in connections
    if(!(id in this.connections)) {
        this.system.log("Id not in connections");
        return null;
    }
    // Make sure the message is parse able
    try {
        message = JSON.parse(message);
    } catch(e) {
        this.system.log("Could not parse message");
        return null;
    }
    // Make sure that the message are in the right format
    if(!("t" in  message && "d" in message)) {
        this.system.log("message is wrong formatT");
        return null;
    }
    return message;
}

/**
 * Handles what happens when a client disconnects.
 * If the client is a player, the player is removed from the box2d world
 *
 * then the connections is deleted from the connections array.
 *
 * @param id
 */
Server.prototype.handleClientClosure = function(id) {
    if(id in this.connections) {
        var connection = this.connections[id];
        if(connection.type == Server.CONNECTION_TYPE_PLAYER) {
            // delete the object from box2d world.
            this.game.world.removeBody(connection.player.body);
            // push the client to the queued Disconnects array
            // to broadcast the client disconnect to other players
            this.queuedDisconnects.push({i: id});
        }
        // delete the connection
        this.system.log("Disconnect from " + this.connections[id].ip + " players in world: " + (this.MathHelper.objectSize(this.connections) - 1));
        delete this.connections[id];
    }
}

/**
 * Creates and starts a server loop which keeps track of
 * the time between frames, and updates the server game.
 *
 * Also sends a game state to all players under the
 * right conditions.
 */
Server.prototype.serverLoop = function() {
    var timeout = 1000/Server.GAME_FPS;
    var lastUpdate = Date.now();
    var that = this;
    var loop = function() {
        var now = Date.now();
        if(lastUpdate + timeout <= now) {
            var deltaTime = (now - lastUpdate) / 1000;
            lastUpdate = now;
            that.game.runGameFrame(deltaTime);
            that.frame = (that.frame + 1) % 20;
            if(that.frame == 0) {
                that.sendGameState();
            }
        }
        if(Date.now() - lastUpdate < timeout - 16) {
            setTimeout(loop);
        } else {
            setImmediate(loop);
        }
    }
    loop();
}

/**
 * Sends the current game state to all connections.
 *
 * Including all world items, chat messages and
 * disconnected users.
 *
 */
Server.prototype.sendGameState = function() {
    var indices = {};
    var playerData = this.getAllPlayers(indices);
    var bytesSent = 0;
    var worldItems = this.game.world.getWorldItems();
    for(var id in this.connections) {
        var connection = this.connections[id];
        if(connection.type == Server.CONNECTION_TYPE_CHART) continue;
        // Subtract chat countdown if there is a chat message
        if(connection.chatMessage) {
            connection.chatCountDown--;
        }
        if(connection.chatCountDown < 1) {
            connection.chatMessage = null;
        }
        var myIndex = indices[id];
        var myPlayer = playerData[myIndex];
        var nearPlayers = {};
        nearPlayers[myIndex] = myPlayer;
        for(var id in playerData) {
            if(id == myIndex) continue;
            var player = playerData[id];
            var xDifference = player.hashGridX - myPlayer.hashGridX;
            var yDifference = player.hashGridY - myPlayer.hashGridY;
            if((xDifference <= 1 && xDifference >= -1) && (yDifference <= 1 && yDifference >= -1)) {
                nearPlayers[id] = player
            }
        }
        var state = JSON.stringify({
            i: myIndex, // myIndex
            p: nearPlayers, // players
            l: this.lastProcessedInput[connection.id], // lpi - last processed input
            r: this.queuedDisconnects, // removes
            m: worldItems, // items
            t: Date.now() // time
        });
        //console.log(Buffer.byteLength(state));
        bytesSent += Buffer.byteLength(state);
        connection.sendUTF(state);
    }
    this.queuedDisconnects = [];

    this.sendCharData(bytesSent);
}

Server.prototype.sendCharData = function(bytesSent) {
    var now = Date.now();
    var deltaTime = (now - this.lastGameSend) / 1000;
    for(var id in this.connections) {
        var connection = this.connections[id];
        if(connection.type != Server.CONNECTION_TYPE_CHART) continue;
        connection.sendUTF(JSON.stringify({
            bytesSent: bytesSent,
            deltaTime: deltaTime
        }));
        break;
    }
    this.lastGameSend = now;
}

/**
 * Get all players and saved their data in a playerData array
 * and return it.
 *
 * @param indices
 * @returns {{}}
 */
Server.prototype.getAllPlayers = function(indices) {
    var playerData = {};
    for(var id in this.connections) {
        var connection = this.connections[id];
        if(!connection.player) continue;
        var data = {};
        if(connection.chatMessage) {
            data.c = connection.chatMessage; // chat message
        }
        data.p = connection.player.getPlayerData(); // PLayer data
        data.a = connection.appliedInputs; // applied inputs
        data.hashGridX = (data.p.p.x / 20) | 0;
        data.hashGridY = (data.p.p.y / 20) | 0;
        connection.appliedInputs = [];
        playerData[id] = data;
        indices[id] = id;
    }
    return playerData;
}

new Server();